# Top Music charts - VUE - JS App

> An example Vue-JS project. Its read data from iTunes Api and display the charts. 

Project is hosted on Gitlab and build and run using CI.
Its a progressive application utilising service workers. 

[View example](https://lececil1.gitlab.io/vuebaxi )

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


