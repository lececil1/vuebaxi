import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/pages/HelloWorld'
import About from '@/pages/Search'
import Category from '@/pages/Artist'
import Artist from '@/pages/Artist2'
import TopSongs from '@/pages/TopSongs'
import TopAlbums from '@/pages/TopAlbums'
import Categories from '@/pages/Categories'
import CategoriesPost from '@/pages/CategoriesPost'
import NotFound from '@/pages/404.vue'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.use(Router)

export default new Router({
    base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: HelloWorld
    },
    {
      path: '/Search',
      name: 'Search',
      component: About
    },
      ,
    {
      path: '/Categories/',
      name: 'Categories',
      component: Categories
    },
    {
      path: '/Categories/:id',
      name: 'Category',
      component: Category
    },
    {
      path: '/Songs/',
      name: 'TopSongs',
      component: TopSongs
    },
    {
      path: '/Albums/',
      name: 'TopAlbums',
      component: TopAlbums
    },
    {
      path: '/Artist',
      name: 'Artist',
      component: Artist
    },
    { 
      path: '*', 
      name: '404',
      component: NotFound 
      }
  ]
})


