module.exports = {
    baseUrl:'https://lececil1.gitlab.io/',
      devServer: {
          public: '35.185.44.232',
          disableHostCheck: true,
          proxy: 'https://lececil1.gitlab.io/'
  },
  publicPath: process.env.NODE_ENV === 'production'
    ? './vuebaxi/'
    : './vuebaxi/'
}